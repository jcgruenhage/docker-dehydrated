FROM docker.io/voidlinux/voidlinux-musl
LABEL maintainer="Jan Christian Grünhage <jan.christian@gruenhage.xyz>"

ENV UID=1337 \
    GID=1337

ADD root/usr/local/bin/install.sh /usr/local/bin/install.sh

RUN xbps-install -Syu \
 && xbps-install -y bash \
 && install.sh

ENV \
    DEHYDRATED_CA="https://acme-staging-v02.api.letsencrypt.org/directory" \
    DEHYDRATED_CHALLENGE="http-01" \
    DEHYDRATED_KEYSIZE="4096" \
    DEHYDRATED_HOOK="" \
    DEHYDRATED_RENEW_DAYS="30" \
    DEHYDRATED_KEY_RENEW="yes" \
    DEHYDRATED_ACCEPT_TERMS="no" \
    DEHYDRATED_EMAIL="user@example.org" \
    DEHYDRATED_GENERATE_CONFIG="yes"

ADD root /

VOLUME /data

CMD ["/bin/s6-svscan", "/etc/s6.d"]
