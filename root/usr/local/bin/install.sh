#!/bin/bash
export builddeps="git python3-pip python3-devel python-pip libffi-devel libressl-devel gcc"
export runtimedeps="python python3 openssl curl sed grep bash s6 su-exec"
xbps-install -y ${builddeps} ${runtimedeps}
git clone https://github.com/lukas2511/dehydrated.git /opt/dehydrated
pip3 install dns-lexicon
pip2 install j2cli[yaml]
xbps-remove -Ry ${builddeps}
